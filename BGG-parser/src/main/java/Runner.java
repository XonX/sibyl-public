import net.sibyl.bgg.SitemapParser;
import net.sibyl.bgg.db.ParsersDAO;
import org.dom4j.DocumentException;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

public class Runner {
  public static void main(String[] args) throws SQLException, MalformedURLException, DocumentException, UnsupportedEncodingException {
    ParsersDAO dao = new ParsersDAO();
    SitemapParser parser = new SitemapParser(dao);
    for (int i = 7; i < 9; i++) {
      URL url = new URL("https://boardgamegeek.com/sitemap_geekitems_boardgame_page_" + i);
      parser.parse(url);
    }
  }
}
