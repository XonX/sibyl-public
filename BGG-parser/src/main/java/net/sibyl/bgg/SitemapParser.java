package net.sibyl.bgg;

import net.sibyl.bgg.db.ParsersDAO;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jooq.util.derby.sys.Sys;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SitemapParser {
  SAXReader reader;
  ParsersDAO dao;

  private final int  MAX_RATINGS_PER_PAGE = 100;
  private final int LAST_SUCCESFULLY_PARSED_GAME_ID = 135451;

  public SitemapParser(ParsersDAO dao) {
    this.dao = dao;
  }

  private Document getConnection(URL url) {
    while (true) {
      try {
        return reader.read(url);
      } catch (DocumentException e) {
        if(e.getMessage().contains("An invalid XML character")) {
          return null;
        }
        try {
          System.out.println(e.getMessage());
          Thread.sleep(3000);
        } catch(InterruptedException ex) {
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  public void parse(URL resource) throws DocumentException {
    reader = new SAXReader();
    Document document = getConnection(resource);

    Map<String, String> uris = new HashMap<>();
    uris.put("mapns", "http://www.sitemaps.org/schemas/sitemap/0.9");
    XPath xpath = document.createXPath("//mapns:url/mapns:loc");
    xpath.setNamespaceURIs(uris);

    List<Node> games = xpath.selectNodes(document);

    for (Node game : games) {
      String gameUrl = game.getText();
      System.out.println("Processing entry " + gameUrl + "...");
      String bggGameId;
      if (gameUrl.lastIndexOf("/") == 34) {
        bggGameId = gameUrl.substring(35);
      }
      else {
        bggGameId = gameUrl.substring(35, gameUrl.lastIndexOf("/"));
      }
      if(Integer.parseInt(bggGameId) > LAST_SUCCESFULLY_PARSED_GAME_ID) {
        parseGame(bggGameId);
        System.out.println("Successfully processed entry: " + gameUrl);
      }
      else {
        System.out.println("Already parsed. Skipping...");
      }
    }
    System.out.println("!!!======sitemap " + resource.getPath() + " processed=====!!!");
  }

  public void parseGame(String gameId) {
    try {
      URL resource = createUrl(gameId);
      Document gameDocument = getConnection(resource);
      if(gameDocument == null) {
        System.out.println("xml corrupt, skipping...");
        return;
      }
      String name = gameDocument.createXPath("//name/@value").valueOf(gameDocument);
      if(dao.isGameExists(name)){
        System.out.println("game exsists, skipping...");
        return;
      }
      String year = gameDocument.createXPath("//yearpublished/@value").valueOf(gameDocument);
      Short minPlayers = parseShortNullSafe(gameDocument.createXPath("//minplayers/@value").valueOf(gameDocument));
      Short maxPlayers = parseShortNullSafe(gameDocument.createXPath("//maxplayers/@value").valueOf(gameDocument));
      Integer playingTime = parseIntNullSafe(gameDocument.createXPath("//playingtime/@value").valueOf(gameDocument));
      Short minAge = parseShortNullSafe(gameDocument.createXPath("//minage/@value").valueOf(gameDocument));

      Date date = null;
      if(year != null && !"".equals(year)) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy");
        date = new Date(formatter.parseDateTime(year).getMillis());
      }

      if("".equals(name)) {
        return;
      }

      dao.saveGame(name, date, minPlayers, maxPlayers, parseShortNullSafe(getBestPlayers(gameDocument)), playingTime,
          minAge, parseShortNullSafe(getRecommendedAge(gameDocument)));

      processAttributes(name, gameDocument);

      int pages = getTotalPages(gameDocument);

      processRatings(gameDocument);
      for(int i = 2; i <= pages; i++) {
        gameDocument = getConnection(createUrl(gameId, i));
        if(gameDocument != null){
          processRatings(gameDocument);
        }
      }
    } catch (MalformedURLException | DocumentException e) {
      e.printStackTrace();
    }
  }

  private void processAttributes(String name, Document gameDocument) {
    XPath linkXpath = gameDocument.createXPath("//link");
    XPath type = gameDocument.createXPath("./@type");
    XPath value = gameDocument.createXPath("./@value");
    List<Node> links = linkXpath.selectNodes(gameDocument);
    for (Node link : links) {
      String linkValue = value.valueOf(link);
      String typeValue = type.valueOf(link);
      Integer typeId = null;
      switch (typeValue) {
        case "boardgamecategory" : typeId = ParsersDAO.CATEGORY; break;
        case "boardgamemechanic" : typeId = ParsersDAO.MECHANIC; break;
        case "boardgameartist" : typeId = ParsersDAO.ARTIST; break;
        case "boardgamedesigner" : typeId = ParsersDAO.DESIGNER; break;
        case "boardgamepublisher" : typeId = ParsersDAO.PUBLISHER; break;
        case "boardgamefamily" : typeId = ParsersDAO.FAMILY;
      }
      if(typeId != null) {
        if(!dao.isKnownLinkValue(linkValue, typeId)) {
          dao.saveLinkValue(linkValue, typeId);
        }
        dao.saveLink(name, linkValue, typeId);
      }
    }
  }

  private Short parseShortNullSafe(String sh) {
    return (sh == null || sh.isEmpty()) ? null :
        sh.contains("+") ? Short.parseShort(sh.substring(0, sh.length() - 1)) : Short.parseShort(sh);
  }

  private Integer parseIntNullSafe(String sh) {
    return (sh == null || sh.isEmpty()) ? null :
        sh.contains("+") ? Integer.parseInt(sh.substring(0, sh.length() - 1)) : Integer.parseInt(sh);
  }


  private void processRatings(Document gameDocument) throws MalformedURLException, DocumentException {
    List<Node> ratings = gameDocument.createXPath("//comment").selectNodes(gameDocument);
    XPath usernameXpath = gameDocument.createXPath("./@username");
    XPath ratingXpath = gameDocument.createXPath("./@rating");
    XPath name = gameDocument.createXPath("//name/@value");
    for(Node rating : ratings) {
      if(!dao.isUserExists(usernameXpath.valueOf(rating))) {
        UserParser userParser = new UserParser(dao);
        userParser.processUser(usernameXpath.valueOf(rating));
      }
      dao.saveRating(name.valueOf(gameDocument), usernameXpath.valueOf(rating), new BigDecimal(ratingXpath.valueOf(rating)));
    }
  }

  private String getRecommendedAge(Document gameDocument) {
    XPath votesXpath = gameDocument.createXPath("//poll[@name='suggested_playerage']//result");
    XPath numvotesXpath = gameDocument.createXPath("./@numvotes");
    List<Node> votes = votesXpath.selectNodes(gameDocument);
    if(votes.isEmpty()) {
      return null;
    }
    Node best = votes.get(0);
    for(Node vote : votes) {
      if(Integer.parseInt(numvotesXpath.valueOf(best)) < Integer.parseInt(numvotesXpath.valueOf(vote))) {
        best = vote;
      }
    }
    return gameDocument.createXPath("./@value").valueOf(gameDocument);
  }

  String getBestPlayers(Document gameDocument) {
    if(gameDocument.createXPath("//poll[@name='suggested_numplayers']/@totalvotes").valueOf(gameDocument).equals("0")) {
      return "";
    }
    XPath votesXpath = gameDocument.createXPath("//results[@numplayers]");
    XPath numvotesXpath = gameDocument.createXPath("./result[@value='Best']/@numvotes");
    List<Node> votes = votesXpath.selectNodes(gameDocument);
    Node best = votes.get(0);
    for(Node vote : votes) {
      if(Integer.parseInt(numvotesXpath.valueOf(vote)) > Integer.parseInt(numvotesXpath.valueOf(best))) {
        best = vote;
      }
    }
    XPath numplayersXpath = gameDocument.createXPath("./@numplayers");
    return numplayersXpath.valueOf(best);
  }

  private int getTotalPages(Document gameDocument) {
    XPath xPath = gameDocument.createXPath("//comments/@totalitems");
    if(xPath.valueOf(gameDocument).isEmpty()) {
      return 1;
    }
    int rates = Integer.parseInt(xPath.valueOf(gameDocument));
    if(rates <= MAX_RATINGS_PER_PAGE) {
      return 1;
    } else {
      return (rates / MAX_RATINGS_PER_PAGE) + 1;
    }
  }

  URL createUrl(String gameId) throws MalformedURLException {
    return new URL("http://www.boardgamegeek.com/xmlapi2/thing?id=" + gameId + "&ratingcomments=1&pagesize=100");
  }

  private URL createUrl(String gameId, int pageNumber) throws MalformedURLException {
    return new URL("http://www.boardgamegeek.com/xmlapi2/thing?id=" + gameId + "&ratingcomments=1&pagesize=100&page=" + pageNumber);
  }
}
