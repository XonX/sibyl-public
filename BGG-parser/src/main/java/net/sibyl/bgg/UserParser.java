package net.sibyl.bgg;

import net.sibyl.bgg.db.ParsersDAO;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.jooq.util.derby.sys.Sys;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

class UserParser {
  String currentUser;

  private ParsersDAO dao;

  UserParser(ParsersDAO dao) {
    this.dao = dao;
  }

  void processUser(String username) throws MalformedURLException, DocumentException {
    System.out.print("Processing user " + username + "...");
    currentUser = username;
    URL url = getUrl(username);
    SAXReader reader = new SAXReader();
    Document document = getConnection(url, reader);
    if(document == null) {
      dao.saveUser(username, null);
      return;
    }
    saveUser(document);
    int pages = getPageNumber(document);
    processFriendList(document);
    for(int i = 2; i <= pages; i++) {
      document = getConnection(getUrl(username, i), reader);
      processFriendList(document);
    }
    System.out.println("Done");
  }

  private Document getConnection(URL url, SAXReader reader) {
    while (true) {
      try {
        return reader.read(url);
      } catch (DocumentException e) {
        if(e.getMessage().contains("HTTP response code: 400")) {
          return null;
        }
        try {
          System.out.println(e.getMessage());
          Thread.sleep(3000);
        } catch(InterruptedException ex) {
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  private URL getUrl(String userName, int pageNumber) throws MalformedURLException {
    return new URL("http://boardgamegeek.com/xmlapi2/user?name=" + userName + "&buddies=1&page=" + pageNumber);
  }

  void processFriendList(Document document) {
    XPath path = document.createXPath("//buddy/@name");
    List<Node> buddies = path.selectNodes(document);

    for(Node buddy : buddies) {
      if(!currentUser.equals(buddy.getStringValue()) && dao.isUserExists(buddy.getStringValue()) && !dao.isUsersFriends(buddy.getStringValue(), currentUser)) {
        dao.saveFriendship(currentUser, buddy.getStringValue());
      }
    }
  }

  private int getPageNumber(Document document) {
    XPath xPath = document.createXPath("//buddies/@total");
    int buddies = Integer.parseInt(xPath.valueOf(document));
    int MAX_BUDDIES_PER_PAGE = 1000;
    if(buddies <= MAX_BUDDIES_PER_PAGE) {
      return 1;
    } else {
      return (buddies / MAX_BUDDIES_PER_PAGE) + 1;
    }
  }

  URL getUrl(String userName) throws MalformedURLException {
    return new URL("http://boardgamegeek.com/xmlapi2/user?name=" + userName + "&buddies=1");
  }

  private void saveUser(Document resource) {
    XPath countryXpath = resource.createXPath("//country/@value");

    dao.saveUser(currentUser, countryXpath.valueOf(resource));
  }
}
