package net.sibyl.bgg.db;

import static net.sibyl.db.Tables.*;

import net.sibyl.dao.DbConnector;
import org.jooq.exception.DataAccessException;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;

public class ParsersDAO extends DbConnector {
  public static int CATEGORY = 1;
  public static int MECHANIC = 2;
  public static int ARTIST = 3;
  public static int DESIGNER = 4;
  public static int PUBLISHER = 5;
  public static int FAMILY = 6;


  public ParsersDAO() throws SQLException {
    super();
  }

  public void saveUser(String name, String country) {
    create.insertInto(USER_ACCOUNT, USER_ACCOUNT.USER_NAME, USER_ACCOUNT.COUNTRY).values(name, country).execute();
  }

  public boolean isUserExists(String name) {
    return !create.select().from(USER_ACCOUNT).where(USER_ACCOUNT.USER_NAME.equal(name)).fetch().isEmpty();
  }

  public boolean isUsersFriends(String name1, String name2) {
    Integer id1 = create.select(USER_ACCOUNT.ID).from(USER_ACCOUNT).where(USER_ACCOUNT.USER_NAME.equal(name1)).fetchAny().value1();
    Integer id2 = create.select(USER_ACCOUNT.ID).from(USER_ACCOUNT).where(USER_ACCOUNT.USER_NAME.equal(name2)).fetchAny().value1();
    return !create.select().from(FRIENDS).where((FRIENDS.FRIEND1_ID.equal(id1).and(FRIENDS.FRIEND2_ID.equal(id2)))
        .or(FRIENDS.FRIEND1_ID.equal(id2).and(FRIENDS.FRIEND2_ID.equal(id1)))).fetch().isEmpty();
  }

  public void saveFriendship(String name1, String name2) {
    Integer id1 = create.select(USER_ACCOUNT.ID).from(USER_ACCOUNT).where(USER_ACCOUNT.USER_NAME.equal(name1)).fetchAny().value1();
    Integer id2 = create.select(USER_ACCOUNT.ID).from(USER_ACCOUNT).where(USER_ACCOUNT.USER_NAME.equal(name2)).fetchAny().value1();
    try {
      create.insertInto(FRIENDS, FRIENDS.FRIEND1_ID, FRIENDS.FRIEND2_ID).values(id1, id2).execute();
    } catch (DataAccessException e) {
      System.out.print("duplicate friendship found, skipping...");
    }

  }

  public void saveGame(String name, Date year, Short minPlayers, Short maxPlayers, Short bestPlayers, Integer playingTime, Short minAge, Short recommendedAge) {
    create.insertInto(GAME, GAME.NAME, GAME.YEARPUBLISHED, GAME.MINPLAYERS, GAME.MAXPLAYERS, GAME.BESTPLAYERS,
        GAME.AVERAGEPLAYINGTIME, GAME.MINAGE, GAME.SUGGESTEDAGE).
        values(name, year, minPlayers, maxPlayers, bestPlayers, playingTime, minAge, recommendedAge).execute();
  }

  public void saveRating(String gameName, String userName, BigDecimal rating) {
    Integer gameId = create.select(GAME.ID).from(GAME).where(GAME.NAME.equal(gameName)).fetchAny().value1();
    Integer userId = create.select(USER_ACCOUNT.ID).from(USER_ACCOUNT).where(USER_ACCOUNT.USER_NAME.equal(userName)).fetchAny().value1();
    try {
      create.insertInto(RATING, RATING.GAME_ID, RATING.USER_ID, RATING.RATING_).values(gameId, userId, rating).execute();
    } catch (DataAccessException e) {
      System.out.println("duplicate rating found, skipping...");
    }
  }


  public void saveLink(String gameName, String linkValue, Integer typeId) {
    Integer gameId = create.select(GAME.ID).from(GAME).where(GAME.NAME.equal(gameName)).fetchAny().value1();
    Integer linkId = create.select(ATTRIBUTE.ID).from(ATTRIBUTE).
        where(ATTRIBUTE.NAME.equal(linkValue).and(ATTRIBUTE.KIND_ID.equal(typeId))).fetchAny().value1();
    try {
      create.insertInto(GAME_ATTRIBUTE, GAME_ATTRIBUTE.GAME_ID, GAME_ATTRIBUTE.ATTRIBUTE_ID).values(gameId, linkId).execute();
    } catch (DataAccessException e) {
      System.out.println("duplicate attribute found, skipping...");
    }
  }

  public void saveLinkValue(String linkValue, Integer typeId) {
    create.insertInto(ATTRIBUTE, ATTRIBUTE.NAME, ATTRIBUTE.KIND_ID).values(linkValue, typeId).execute();
  }

  public boolean isKnownLinkValue(String linkValue, Integer typeId) {
    return !create.select().from(ATTRIBUTE).where(ATTRIBUTE.NAME.equal(linkValue).and(ATTRIBUTE.KIND_ID.equal(typeId))).fetch().isEmpty();
  }

  public boolean isGameExists(String name) {
    return !create.select().from(GAME).where(GAME.NAME.equal(name)).fetch().isEmpty();
  }
}
