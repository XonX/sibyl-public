package net.sibyl.bgg;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import net.sibyl.bgg.db.ParsersDAO;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;

public class SitemapParserTest {
  SitemapParser sitemapParser;
  ParsersDAO dao;

  @Before
  public void setUp() {
    dao = mock(ParsersDAO.class);
    sitemapParser = new SitemapParser(dao);
  }

  @Test
  public void testParseSitemap() throws Exception {
    sitemapParser = spy(sitemapParser);
    doNothing().when(sitemapParser).parseGame(anyString());

    sitemapParser.parse(getClass().getResource("/sitemap_geekitems_boardgame_page_1"));

    verify(sitemapParser).parseGame("1");
    verify(sitemapParser).parseGame("2392");
  }

  @Test
  public void testCreateUrl() throws Exception {
    URL url1 = sitemapParser.createUrl("1");
    URL url2 = sitemapParser.createUrl("500");

    assertEquals(new URL("http://www.boardgamegeek.com/xmlapi2/thing?id=1&ratingcomments=1&pagesize=100"), url1);
    assertEquals(new URL("http://www.boardgamegeek.com/xmlapi2/thing?id=500&ratingcomments=1&pagesize=100"), url2);
  }

  @Test
  public void testGetBestPlayers() throws Exception {
    SAXReader reader = new SAXReader();
    Document document = reader.read(getClass().getResource("/thing?id=1&ratingcomments=1"));

    assertEquals("5", sitemapParser.getBestPlayers(document));
  }
}
