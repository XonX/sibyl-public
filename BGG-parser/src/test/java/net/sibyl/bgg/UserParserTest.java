package net.sibyl.bgg;

import net.sibyl.bgg.db.ParsersDAO;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class UserParserTest {
  private UserParser parser;
  private ParsersDAO dao;

  @Before
  public void setUp() {
    dao = mock(ParsersDAO.class);
    parser = new UserParser(dao);
  }

  @Test
  public void testProcessUser() throws Exception {
    String userName = "Ashlar";
    parser = spy(parser);
    when(parser.getUrl(userName)).thenReturn(getClass().getResource("/" + userName));

    parser.processUser("Ashlar");

    verify(dao).saveUser("Ashlar", "Estonia");
  }

  @Test
  public void testGetUserUrl() throws Exception {
    URL url = parser.getUrl("Ashlar");
    assertEquals("http://boardgamegeek.com/xmlapi2/user?name=Ashlar&buddies=1", url.toString());
  }

  @Test
  public void testProcessFriendsList() throws Exception {
    SAXReader reader = new SAXReader();
    Document document = reader.read(getClass().getResource("/Ashlar"));
    when(dao.isUserExists("Furry")).thenReturn(true);
    when(dao.isUserExists("Reitser")).thenReturn(true);
    when(dao.isUsersFriends(anyString(), anyString())).thenReturn(false).thenReturn(true);

    parser.currentUser = "Ashlar";

    parser.processFriendList(document);

    verify(dao, times(43)).isUserExists(anyString());
    verify(dao, times(2)).isUsersFriends(anyString(), anyString());
    verify(dao).saveFriendship(anyString(), anyString());
  }
}
