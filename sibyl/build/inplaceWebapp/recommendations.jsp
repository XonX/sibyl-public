<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  </head>
  <body>
    <table>
      <tr>
        <th>Name</th>
        <th>Est. score</th>
      </tr>
      <c:forEach var="game" items="${recommendedGames}">
          <tr>
            <td>
              <c:out value="${game.key}"/>
            </td>
            <td>
              <c:out value="${game.value}"/>
            </td>
          </tr>
      </c:forEach>
    </table>
  </body>
</html>