CREATE TABLE user_account (
    id serial primary key,
    user_name VARCHAR(100)
);
create unique index user_account_idx on user_account using btree (user_name);