CREATE TABLE game (
    id serial PRIMARY KEY,
    name VARCHAR(200),
    yearPublished DATE,
    minPlayers SMALLINT,
    maxPlayers SMALLINT,
    bestPlayers SMALLINT,
    recommendedPlayers SMALLINT[],
    averagePlayingTime SMALLINT,
    minPlayingTime SMALLINT,
    maxPlayingTime SMALLINT,
    minAge SMALLINT,
    suggestedAge SMALLINT
    --links here
)