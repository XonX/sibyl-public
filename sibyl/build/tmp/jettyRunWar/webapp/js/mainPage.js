function addUser () {
  var amount = $("#amountField").val();
  amount++;
  $("#amountField").val(amount);
  $("#userAddButton").before("User " + amount + ": <input type='text' id='user" + amount + "' class='user'/><br/>");
}

function validateForm() {
  var hasUserNames = $(".user").filter(function() { return $(this).val(); }).length > 0;
  if(!hasUserNames) {
    $("#errorContainer").html("Please provide at least 1 username");
  }
  return hasUserNames;
}
