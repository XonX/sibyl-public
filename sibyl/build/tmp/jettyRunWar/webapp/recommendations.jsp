<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  </head>
  <body>
    <table>
      <th>
        <td>Name</td>
      </th>
      <c:forEach var="game" items="${games}">
          <tr>
            <td>
              <c:out value="${game}"/>
            </td>
          </tr>
      </c:forEach>
    </table>
  </body>
</html>