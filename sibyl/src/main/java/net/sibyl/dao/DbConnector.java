package net.sibyl.dao;

import static net.sibyl.db.Tables.*;

import net.sibyl.db.tables.pojos.Game;
import net.sibyl.db.tables.pojos.UserAccount;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {
  protected DSLContext create;

  public DbConnector() throws SQLException {
    String userName = "user";
    String password = "password";
    String url = "jdbc:postgresql://localhost/sibyl";

    Connection conn = DriverManager.getConnection(url, userName, password);
    create = DSL.using(conn, SQLDialect.POSTGRES);
  }

  public DSLContext getContext() {
    return create;
  }

  BigInteger getGameCount() {
    return create.selectCount().from(GAME).fetchOne(0, BigInteger.class);
  }

  public Game getGameById(Long itemID) {
    return create.select().from(GAME).where(GAME.ID.equal(itemID.intValue())).fetchOne().into(Game.class);
  }

  public UserAccount getUserByName(String name) {
    return create.select().from(USER_ACCOUNT).where(USER_ACCOUNT.USER_NAME.equal(name)).fetchOne().into(UserAccount.class);
  }

}
