/**
 * This class is generated by jOOQ
 */
package net.sibyl.db.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import net.sibyl.db.Keys;
import net.sibyl.db.Public;
import net.sibyl.db.tables.records.FriendsRecord;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.0"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Friends extends TableImpl<FriendsRecord> {

	private static final long serialVersionUID = -1154056662;

	/**
	 * The reference instance of <code>public.friends</code>
	 */
	public static final Friends FRIENDS = new Friends();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<FriendsRecord> getRecordType() {
		return FriendsRecord.class;
	}

	/**
	 * The column <code>public.friends.friend1_id</code>.
	 */
	public final TableField<FriendsRecord, Integer> FRIEND1_ID = createField("friend1_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

	/**
	 * The column <code>public.friends.friend2_id</code>.
	 */
	public final TableField<FriendsRecord, Integer> FRIEND2_ID = createField("friend2_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

	/**
	 * Create a <code>public.friends</code> table reference
	 */
	public Friends() {
		this("friends", null);
	}

	/**
	 * Create an aliased <code>public.friends</code> table reference
	 */
	public Friends(String alias) {
		this(alias, FRIENDS);
	}

	private Friends(String alias, Table<FriendsRecord> aliased) {
		this(alias, aliased, null);
	}

	private Friends(String alias, Table<FriendsRecord> aliased, Field<?>[] parameters) {
		super(alias, Public.PUBLIC, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<FriendsRecord> getPrimaryKey() {
		return Keys.FRIENDS_PKEY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<FriendsRecord>> getKeys() {
		return Arrays.<UniqueKey<FriendsRecord>>asList(Keys.FRIENDS_PKEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ForeignKey<FriendsRecord, ?>> getReferences() {
		return Arrays.<ForeignKey<FriendsRecord, ?>>asList(Keys.FRIENDS__FRIENDS_FRIEND1_ID_FKEY, Keys.FRIENDS__FRIENDS_FRIEND2_ID_FKEY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Friends as(String alias) {
		return new Friends(alias, this);
	}

	/**
	 * Rename this table
	 */
	public Friends rename(String name) {
		return new Friends(name, null);
	}
}
