/**
 * This class is generated by jOOQ
 */
package net.sibyl.db.tables.pojos;


import java.io.Serializable;
import java.math.BigDecimal;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.0"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Rating implements Serializable {

	private static final long serialVersionUID = -1722332465;

	private final Integer    userId;
	private final Integer    gameId;
	private final BigDecimal rating;

	public Rating(Rating value) {
		this.userId = value.userId;
		this.gameId = value.gameId;
		this.rating = value.rating;
	}

	public Rating(
		Integer    userId,
		Integer    gameId,
		BigDecimal rating
	) {
		this.userId = userId;
		this.gameId = gameId;
		this.rating = rating;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public Integer getGameId() {
		return this.gameId;
	}

	public BigDecimal getRating() {
		return this.rating;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Rating (");

		sb.append(userId);
		sb.append(", ").append(gameId);
		sb.append(", ").append(rating);

		sb.append(")");
		return sb.toString();
	}
}
