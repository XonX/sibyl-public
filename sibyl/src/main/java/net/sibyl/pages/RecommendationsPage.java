package net.sibyl.pages;

import net.sibyl.aggregators.AverageAggregator;
import net.sibyl.dao.DbConnector;
import net.sibyl.db.tables.pojos.Game;
import net.sibyl.db.tables.pojos.UserAccount;
import net.sibyl.recommenders.AggregatingRecommender;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.jdbc.PostgreSQLJDBCDataModel;
import org.apache.mahout.cf.taste.impl.model.jdbc.ReloadFromJDBCDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.CachingUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.CachingUserSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.postgresql.ds.PGPoolingDataSource;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class RecommendationsPage extends HttpServlet {
  private AggregatingRecommender recommender;
  private DbConnector connector;

  @Override
  public void init(ServletConfig config) {
    try {
      PGPoolingDataSource source = new PGPoolingDataSource();
      source.setDataSourceName("tasteDb");
      source.setServerName("localhost");
      source.setDatabaseName("sibyl");
      source.setUser("user");
      source.setPassword("password");
      DataModel model = new ReloadFromJDBCDataModel(new PostgreSQLJDBCDataModel(source, "rating", "user_id", "game_id", "rating", null));
      UserSimilarity similarity = new CachingUserSimilarity(new EuclideanDistanceSimilarity(model), model);
      UserNeighborhood neighborhood = new CachingUserNeighborhood(new NearestNUserNeighborhood(100, similarity, model), model);
      System.out.println("Initialising recommender...");
      Recommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
      this.recommender = new AggregatingRecommender(recommender, new AverageAggregator());
      System.out.println("Recommender initialised");

      connector = new DbConnector();
    } catch (TasteException | SQLException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String[] userNames = request.getParameterValues("user");
    List<Long> userIds = new ArrayList<>();
    for (String userName : userNames) {
      UserAccount user = connector.getUserByName(userName);
      userIds.add(user.getId().longValue());
    }

    List<RecommendedItem> recommendedItems = Collections.emptyList();

    try {
      recommendedItems = recommender.recommend(userIds, 20);
    } catch (TasteException e) {
      e.printStackTrace();
    }
    Map<String, Float> recommendedGames = new HashMap<>();
    for(RecommendedItem item : recommendedItems) {
      Game game = connector.getGameById(item.getItemID());
      recommendedGames.put(game.getName(), item.getValue());
    }

    request.setAttribute("recommendedGames", recommendedGames);

    request.getRequestDispatcher("/recommendations.jsp").forward(request, response);
  }
}
