package net.sibyl.recommenders;

import aggregators.Aggregator;
import org.apache.mahout.cf.taste.common.Refreshable;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.recommender.ByValueRecommendedItemComparator;
import org.apache.mahout.cf.taste.impl.recommender.GenericRecommendedItem;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.IDRescorer;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import recommenders.GroupRecommender;

import java.util.*;
import java.util.stream.Collectors;

public class AggregatingRecommender implements GroupRecommender {
  private Recommender baseRecommender;
  private Aggregator aggregator;
  private ByValueRecommendedItemComparator comparator;

  public AggregatingRecommender(Recommender baseRecommender, Aggregator aggregator) {
    this.baseRecommender = baseRecommender;
    this.aggregator = aggregator;
    comparator = new ByValueRecommendedItemComparator();
  }

  @Override
  public List<RecommendedItem> recommend(Collection<Long> userIDs, int howMany) throws TasteException {
    List<RecommendedItem> gathered = new ArrayList<>();
    for(long userId : userIDs) {
      gathered.addAll(recommend(userId, howMany));
    }
    Set<Long> itemIds = gathered.stream().map(RecommendedItem::getItemID).collect(Collectors.toSet());
    List<RecommendedItem> result = new ArrayList<>();
    for(long itemId : itemIds) {
      List<Float> ratings = new ArrayList<>();
      for(long userId : userIDs) {
        ratings.add(estimatePreference(userId, itemId));
      }
      result.add(new GenericRecommendedItem(itemId, aggregator.aggregate(ratings)));
    }
    result.sort(comparator);
    return result.subList(0, howMany);
  }

  @Override
  public float estimatePreference(Collection<Long> userIDs, long itemID) throws TasteException {
    DataModel model = getDataModel();
    List<Float> ratings = new ArrayList<>();
     for(long userId : userIDs) {
      ratings.add(estimatePreference(userId, itemID));
    }
    return aggregator.aggregate(ratings);
  }

  @Override
  public List<RecommendedItem> recommend(long userID, int howMany) throws TasteException {
    return baseRecommender.recommend(userID, howMany);
  }

  @Override
  public List<RecommendedItem> recommend(long userID, int howMany, IDRescorer rescorer) throws TasteException {
    return baseRecommender.recommend(userID, howMany, rescorer);
  }

  @Override
  public float estimatePreference(long userID, long itemID) throws TasteException {
    return baseRecommender.estimatePreference(userID, itemID);
  }

  @Override
  public void setPreference(long userID, long itemID, float value) throws TasteException {
    baseRecommender.setPreference(userID, itemID, value);
  }

  @Override
  public void removePreference(long userID, long itemID) throws TasteException {
    baseRecommender.removePreference(userID, itemID);
  }

  @Override
  public DataModel getDataModel() {
    return baseRecommender.getDataModel();
  }

  @Override
  public void refresh(Collection<Refreshable> alreadyRefreshed) {
    baseRecommender.refresh(alreadyRefreshed);
  }
}
