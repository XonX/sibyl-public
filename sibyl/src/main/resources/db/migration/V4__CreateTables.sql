CREATE TABLE rating (
  user_id int REFERENCES user_account (id) ON UPDATE CASCADE ON DELETE CASCADE,
  game_id int REFERENCES game (id) ON UPDATE CASCADE ON DELETE CASCADE,
  rating SMALLINT,
  CONSTRAINT rating_pkey PRIMARY KEY (user_id, game_id)
);

CREATE TABLE attribute_kind (
  id serial PRIMARY KEY,
  kind VARCHAR(30) UNIQUE
);

INSERT INTO attribute_kind (kind)
VALUES ('category');

INSERT INTO attribute_kind (kind)
VALUES ('mechanic');

INSERT INTO attribute_kind (kind)
VALUES ('artist');

INSERT INTO attribute_kind (kind)
VALUES ('designer');

INSERT INTO attribute_kind (kind)
VALUES ('publisher');

CREATE TABLE attribute (
  id serial PRIMARY KEY,
  name VARCHAR(150),
  kind_id int REFERENCES attribute_kind(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE game_attribute (
  attribute_id int REFERENCES attribute (id) ON UPDATE CASCADE ON DELETE CASCADE,
  game_id int REFERENCES game (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT attr_pkey PRIMARY KEY (attribute_id, game_id)
);

CREATE TABLE friends (
  friend1_id int REFERENCES user_account (id) ON UPDATE CASCADE ON DELETE CASCADE,
  friend2_id int REFERENCES user_account (id) ON UPDATE CASCADE ON DELETE CASCADE,
  constraint friends_pkey PRIMARY KEY (friend1_id, friend2_id)
);