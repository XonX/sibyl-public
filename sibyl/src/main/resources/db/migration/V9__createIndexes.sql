create index gamename_index on game (name);

create index friends_index on friends (friend1_id, friend2_id);