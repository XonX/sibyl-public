<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="greeter" class="net.sibyl.pages.Main"/>
<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="js/mainPage.js"></script>
  </head>
  <body>
    <div id="errorContainer"></div>
    <form action="recommendations" onsubmit="return validateForm()" method="POST">
      <input type="hidden" name="userAmount" id="amountField" value="1"/>
      User 1: <input type="text" name="user" class="user"/><br/>
      <button id="userAddButton" type="button" onclick="addUser();">Add User</button><br/>
      <input type="submit" value="Generate Recommendations"/>
    </form>
  </body>
</html>