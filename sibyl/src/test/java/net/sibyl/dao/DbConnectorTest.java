package net.sibyl.dao;

import net.sibyl.db.tables.pojos.Game;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DbConnectorTest {
  DbConnector connector;

  @Before
  public void setUp() throws Exception {
    connector = new DbConnector();
  }

  @Test
  public void testGetGameCount() throws Exception {
    assertNotNull(connector.getGameCount());
  }

}