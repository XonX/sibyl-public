import aggregators.LeastMiseryAggregator;
import dao.GroupDataProvider;
import dao.TestSuitDao;
import evaluators.AggregateEvaluator;
import aggregators.Aggregator;
import net.sibyl.aggregators.AverageAggregator;
import net.sibyl.recommenders.AggregatingRecommender;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.model.jdbc.PostgreSQLJDBCDataModel;
import org.apache.mahout.cf.taste.impl.model.jdbc.ReloadFromJDBCDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.*;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.postgresql.ds.PGPoolingDataSource;

import java.sql.SQLException;

public class TestSuit {
  public static void main(String[] args) throws TasteException, SQLException {
    PGPoolingDataSource source = new PGPoolingDataSource();
    source.setDataSourceName("tasteDb");
    source.setServerName("localhost");
    source.setPortNumber(5432);
    source.setDatabaseName("sibyl");
    source.setUser("user");
    source.setPassword("password");
    DataModel model = new ReloadFromJDBCDataModel(new PostgreSQLJDBCDataModel(source, "rating", "user_id", "game_id", "rating", null));
    GroupDataProvider dao = new TestSuitDao();

    Aggregator averageAggregator = new AverageAggregator();

//    RecommenderBuilder builder = dataModel -> {
//      UserSimilarity similarity = new CityBlockSimilarity(dataModel);
//      UserNeighborhood neighborhood = new NearestNUserNeighborhood(100, similarity, dataModel);
//      Recommender recommender = new RandomRecommender(dataModel);
//      return new AggregatingUserBasedRecommender(recommender, averageAggregator);
//    };
//
    AggregateEvaluator evaluator = new AggregateEvaluator();
//
//    System.out.println("====Beginning evaluation of random recommender with AverageAggregator====");
//
//    evaluator.evaluate(builder, null, model, 0.8, 0.2, 5);
//    evaluator.evaluate(builder, null, model, 0.8, 0.2, 4);
//    evaluator.evaluate(builder, null, model, 0.8, 0.2, 3);
//    evaluator.evaluate(builder, null, model, 0.8, 0.2, 2);


    RecommenderBuilder builder = dataModel -> {
      ItemSimilarity similarity = new CityBlockSimilarity(dataModel);
      Recommender recommender = new GenericItemBasedRecommender(dataModel, similarity);
      return new AggregatingRecommender(recommender, averageAggregator);
    };



    System.out.println("====Beginning evaluation of User based recommender with AverageAggregator====");

    evaluator.evaluate(builder, null, model, 0.8, 0.2, 5, dao);
    evaluator.evaluate(builder, null, model, 0.8, 0.2, 4, dao);
    evaluator.evaluate(builder, null, model, 0.8, 0.2, 3, dao);
    evaluator.evaluate(builder, null, model, 0.8, 0.2, 2, dao);

    Aggregator leastMiseryAggregator = new LeastMiseryAggregator();

    builder = dataModel -> {
      ItemSimilarity similarity = new CityBlockSimilarity(dataModel);
      Recommender recommender = new GenericItemBasedRecommender(dataModel, similarity);
      return new AggregatingRecommender(recommender, leastMiseryAggregator);
    };

    System.out.println("====Beginning evaluation of User based recommender with LeastMiseryAggregator====");

    evaluator.evaluate(builder, null, model, 0.8, 0.2, 5, dao);
    evaluator.evaluate(builder, null, model, 0.8, 0.2, 4, dao);
    evaluator.evaluate(builder, null, model, 0.8, 0.2, 3, dao);
    evaluator.evaluate(builder, null, model, 0.8, 0.2, 2, dao);

  }

}
