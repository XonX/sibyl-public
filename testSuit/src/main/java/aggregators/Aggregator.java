package aggregators;

import java.util.List;

public interface Aggregator {
  float aggregate(List<Float> ratings);
}
