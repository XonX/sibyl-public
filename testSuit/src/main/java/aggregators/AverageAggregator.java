package aggregators;

import java.util.List;

public class AverageAggregator implements Aggregator {
  @Override
  public float aggregate(List<Float> ratings) {
    return (float) ratings.stream().filter(a -> !Float.isNaN(a)).mapToDouble(a -> a).average().orElse(Double.NaN);
  }
}
