package aggregators;

import java.util.Comparator;
import java.util.List;

public class LeastMiseryAggregator implements Aggregator {
  @Override
  public float aggregate(List<Float> ratings) {
    return ratings.stream().min(Comparator.naturalOrder()).orElse(Float.NaN);
  }
}
