package dao;

import java.util.List;

public interface GroupDataProvider {
  List<Integer> getGroupForUser(int userId, int howBig);
}
