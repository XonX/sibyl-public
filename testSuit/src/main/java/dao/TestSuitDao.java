package dao;

import net.sibyl.dao.DbConnector;
import org.jooq.AggregateFunction;
import org.jooq.impl.DSL;

import java.sql.SQLException;
import java.util.List;

import static net.sibyl.db.Tables.RATING;
import static net.sibyl.db.Tables.USER_ACCOUNT;

public class TestSuitDao extends DbConnector implements GroupDataProvider {
  public TestSuitDao() throws SQLException {
    super();
  }

  @Override
  public List<Integer> getGroupForUser(int userId, int howBig) {
    AggregateFunction<Integer> count = DSL.count();
    List<Integer> userIds = create.select(RATING.USER_ID, count).from(RATING).where(RATING.GAME_ID.in(
        create.select(RATING.GAME_ID).from(RATING).join(USER_ACCOUNT).on(USER_ACCOUNT.ID.equal(RATING.USER_ID)).where(USER_ACCOUNT.ID.equal(userId)))
    ).groupBy(RATING.USER_ID).orderBy(count.desc()).limit(howBig).fetch(0, Integer.class);
    return userIds;
  }
}
