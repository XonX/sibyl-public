package evaluators;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import dao.GroupDataProvider;
import dao.TestSuitDao;
import recommenders.GroupRecommender;
import org.apache.commons.lang.ArrayUtils;
import org.apache.mahout.cf.taste.common.NoSuchItemException;
import org.apache.mahout.cf.taste.common.NoSuchUserException;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.DataModelBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.common.*;
import org.apache.mahout.cf.taste.impl.eval.AbstractDifferenceRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.GenericDataModel;
import org.apache.mahout.cf.taste.impl.model.GenericPreference;
import org.apache.mahout.cf.taste.impl.model.GenericUserPreferenceArray;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.Preference;
import org.apache.mahout.cf.taste.model.PreferenceArray;
import org.apache.mahout.common.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.apache.commons.lang.ArrayUtils.INDEX_NOT_FOUND;
import static org.apache.commons.lang.ArrayUtils.indexOf;

public class AggregateEvaluator extends AbstractDifferenceRecommenderEvaluator {
  private static final Logger log = LoggerFactory.getLogger(AggregateEvaluator.class);

  private final Random random;

  private RunningAverage average;
  private GroupDataProvider groupDataProvider;

  public AggregateEvaluator() throws SQLException {
    random = RandomUtils.getRandom();
  }

  @Override
  protected void reset() {
    average = new FullRunningAverage();
  }

  @Override
  protected void processOneEstimate(float estimatedPreference, Preference realPref) {
    average.addDatum(Math.sqrt(Math.pow(realPref.getValue() - estimatedPreference, 2)));
  }

  protected void processGroupEstimate(float estimatedPreference, List<Float> realPrefs, int size) {
    double sum = 0;
    for(float pref : realPrefs) {
      if(!Float.isNaN(pref)) {
        sum += Math.pow(estimatedPreference - pref, 2);
      }
      else {
        size--;
      }
    }
    average.addDatum(Math.sqrt(sum / size));
  }

  @Override
  protected double computeFinalEvaluation() {
    return average.getAverage();
  }

  @Override
  public String toString() {
    return "AverageAbsoluteDifferenceRecommenderEvaluator";
  }

  public double evaluate(RecommenderBuilder recommenderBuilder,
                         DataModelBuilder dataModelBuilder,
                         DataModel dataModel,
                         double trainingPercentage,
                         double evaluationPercentage,
                         int maxGroupSize,
                         GroupDataProvider groupDataProvider) throws TasteException {
    Preconditions.checkNotNull(recommenderBuilder);
    Preconditions.checkNotNull(dataModel);
    Preconditions.checkArgument(trainingPercentage >= 0.0 && trainingPercentage <= 1.0,
        "Invalid trainingPercentage: " + trainingPercentage + ". Must be: 0.0 <= trainingPercentage <= 1.0");
    Preconditions.checkArgument(evaluationPercentage >= 0.0 && evaluationPercentage <= 1.0,
        "Invalid evaluationPercentage: " + evaluationPercentage + ". Must be: 0.0 <= evaluationPercentage <= 1.0");
    Preconditions.checkNotNull(groupDataProvider);

    this.groupDataProvider = groupDataProvider;
    log.info("Beginning evaluation using {} of {}", trainingPercentage, dataModel);

    int numUsers = dataModel.getNumUsers();
    FastByIDMap<PreferenceArray> trainingPrefs = new FastByIDMap<>(
        1 + (int) (evaluationPercentage * numUsers));
    Map<Long, FastByIDMap<PreferenceArray>> testPrefs = new HashMap<>();

    Set<Long> usedIDs = new HashSet<>();
    LongPrimitiveIterator it = dataModel.getUserIDs();
    int usersProcessed = 0;
    while (it.hasNext()) {
      long userID = it.nextLong();
      if (random.nextDouble() < evaluationPercentage && !usedIDs.contains(userID)) {
        splitGroupUsersPrefs(trainingPercentage, trainingPrefs, testPrefs, userID, dataModel, maxGroupSize);
        usedIDs.add(userID);
        if(testPrefs.get(userID) != null) {
          testPrefs.get(userID).keySetIterator().forEachRemaining(usedIDs::add);
        }
        if(usedIDs.size() - 1000 >= usersProcessed) {
          usersProcessed += 1000;
          log.info(usedIDs.size() + " users processed in splitting");
        }

      }
    }
    //copy the while loop, change splitOneUsersPrefs to splitGroupUsersPrefs method, add a list of IDs already used, fill it with ids of ingroup users. Skip IDs in the list
    DataModel trainingModel = dataModelBuilder == null ? new GenericDataModel(trainingPrefs)
        : dataModelBuilder.buildDataModel(trainingPrefs);

    GroupRecommender recommender = (GroupRecommender) recommenderBuilder.buildRecommender(trainingModel);
    //copy training model and recommender creation
    double result = getEvaluation(testPrefs, recommender);
    log.info("Evaluation result: {}", result);
    return result;
    //call custom getEvaluation method
  }

  private double getEvaluation(Map<Long,FastByIDMap<PreferenceArray>> testPrefs, GroupRecommender recommender)
      throws TasteException {
    reset();
    Collection<Callable<Void>> estimateCallables = Lists.newArrayList();
    AtomicInteger noEstimateCounter = new AtomicInteger();
    estimateCallables.addAll(testPrefs.entrySet()
        .stream()
        .map(entry -> new AggregatedPreferenceEstimateCallable(recommender, entry.getValue(), noEstimateCounter))
        .collect(Collectors.toList()));
    log.info("Beginning evaluation of {} users", estimateCallables.size());
    RunningAverageAndStdDev timing = new FullRunningAverageAndStdDev();
    execute(estimateCallables, noEstimateCounter, timing);
    return computeFinalEvaluation();
  }

  private void splitGroupUsersPrefs(double trainingPercentage,
                                    FastByIDMap<PreferenceArray> trainingPrefs,
                                    Map<Long, FastByIDMap<PreferenceArray>> testPrefs,
                                    long userID,
                                    DataModel dataModel,
                                    int maxGroupSize) throws TasteException {
    List<Integer> groupForUser = groupDataProvider.getGroupForUser((int) userID, maxGroupSize);

    Map<Integer, List<Preference>> groupTrainingPrefs = null;
    Map<Integer, List<Preference>> groupTestPrefs = null;
    Map<Integer, PreferenceArray> prefs = new HashMap<>();
    for(int groupUserId : groupForUser) {
      prefs.put(groupUserId, dataModel.getPreferencesFromUser(groupUserId));
    }
    PreferenceArray seedingPrefs = dataModel.getPreferencesFromUser(userID);
    for (int i = 0; i < seedingPrefs.length(); i++) {
      long itemId = seedingPrefs.getItemID(i);
      if (random.nextDouble() < trainingPercentage) {
        if(groupTrainingPrefs == null) {
          groupTrainingPrefs = new HashMap<>();
          for (Integer groupUserId : groupForUser) {
            groupTrainingPrefs.put(groupUserId, Lists.newArrayListWithCapacity(3));
          }
        }
        for (int groupUserId : groupForUser) {
          PreferenceArray groupUserPrefs = prefs.get(groupUserId);
          int index = indexOf(groupUserPrefs.getIDs(), itemId);
          if(index != INDEX_NOT_FOUND) {
            groupTrainingPrefs.get(groupUserId).add(new GenericPreference(groupUserId, itemId, groupUserPrefs.getValue(index)));
          }
          else {
            groupTrainingPrefs.get(groupUserId).add(new EmptyPreference(groupUserId, itemId));
          }
        }
      } else {
        if(groupTestPrefs == null) {
          groupTestPrefs = new HashMap<>();
          for (Integer groupUserId : groupForUser) {
            groupTestPrefs.put(groupUserId, Lists.newArrayListWithCapacity(3));
          }
        }
        for (int groupUserId : groupForUser) {
          PreferenceArray groupUserPrefs = prefs.get(groupUserId);
          int index = indexOf(groupUserPrefs.getIDs(), itemId);
          if(index != INDEX_NOT_FOUND) {
            groupTestPrefs.get(groupUserId).add(new GenericPreference(groupUserId, itemId, groupUserPrefs.getValue(index)));
          }
          else {
            groupTestPrefs.get(groupUserId).add(new EmptyPreference(groupUserId, itemId));
          }
        }
      }
      if(groupTrainingPrefs != null) {
        for (int groupUserId : groupForUser)
          if (groupUserId == userID) {
            trainingPrefs.put(groupUserId, new GenericUserPreferenceArray(groupTrainingPrefs.get(groupUserId)));
          } else if (groupTestPrefs == null) {
            trainingPrefs.put(groupUserId, prefs.get(groupUserId).clone());
          } else {
            List<Preference> groupUserPreferences = new ArrayList<>();
            for (Preference preference : prefs.get(groupUserId)) {
              if(!groupTestPrefs.get(groupUserId).contains(preference)) {
                groupUserPreferences.add(preference);
              }
            }
            trainingPrefs.put(groupUserId, new GenericUserPreferenceArray(groupUserPreferences));
          }
        if(groupTestPrefs != null) {
          testPrefs.put(userID, new FastByIDMap<>(maxGroupSize));
          for (int groupUserId : groupForUser) {
            testPrefs.get(userID).put(groupUserId, new GenericUserPreferenceArray(groupTestPrefs.get(groupUserId)));
          }
        }
      }
    }
  }

  public class AggregatedPreferenceEstimateCallable implements Callable<Void> {
    private final GroupRecommender recommender;
    private final FastByIDMap<PreferenceArray> prefs;
    private final AtomicInteger noEstimateCounter;

    public AggregatedPreferenceEstimateCallable(GroupRecommender recommender,
                                                FastByIDMap<PreferenceArray> prefs,
                                                AtomicInteger noEstimateCounter) {
      this.recommender = recommender;
      this.prefs = prefs;
      this.noEstimateCounter = noEstimateCounter;
    }

    @Override
    public Void call() throws Exception {
      try {
        Set<Long> itemIds = new HashSet<>();
        for (PreferenceArray preferenceArray : prefs.values()) {
          for (Preference preference : preferenceArray) {
            itemIds.add(preference.getItemID());
          }
        }
        List<Long> group = prefs.values()
            .stream()
            .map(preferences -> preferences.getUserID(0))
            .collect(Collectors.toList());
        for (long itemId : itemIds) {
          float estimatedPreference = Float.NaN;
          try {
            estimatedPreference = recommender.estimatePreference(group, itemId);
          } catch (NoSuchUserException nsue) {
            // It's possible that an item exists in the test data but not training data in which case
            // NSEE will be thrown. Just ignore it and move on.
            log.info("Users exist in test data but not training data: {}", group);
          } catch (NoSuchItemException nsie) {
            log.info("Item exists in test data but not training data: {}", itemId);
          }
          if (Float.isNaN(estimatedPreference)) {
            noEstimateCounter.incrementAndGet();
          } else {
            List<Float> realPrefs = prefs.values()
                .stream()
                .map(preferences ->
                    getPreferenceId(preferences, itemId) == -1 ?
                    Float.NaN
                    : preferences.getValue(getPreferenceId(preferences, itemId)))
                .collect(Collectors.toList());
            processGroupEstimate(estimatedPreference, realPrefs, prefs.size());
          }
        }
      } catch (Exception ex) {
        log.info("debug meh plz");
        throw ex;
      }
      return null;
    }

    private int getPreferenceId(PreferenceArray preferences, long itemId) {
      return Arrays.asList(ArrayUtils.toObject(preferences.getIDs())).indexOf(itemId);
    }
  }
}
