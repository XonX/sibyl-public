package evaluators;

import org.apache.mahout.cf.taste.model.Preference;

class EmptyPreference implements Preference {

  private int userId;
  private long itemId;

  public EmptyPreference(int userId, long itemId) {
    this.userId = userId;
    this.itemId = itemId;
  }

  @Override
  public long getUserID() {
    return userId;
  }

  @Override
  public long getItemID() {
    return itemId;
  }

  @Override
  public float getValue() {
    return Float.NaN;
  }

  @Override
  public void setValue(float value) {

  }
}
