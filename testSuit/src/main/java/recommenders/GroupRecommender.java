package recommenders;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;

import java.util.Collection;
import java.util.List;

public interface GroupRecommender extends Recommender {
  List<RecommendedItem> recommend(Collection<Long> userIDs, int howMany) throws TasteException;

  float estimatePreference(Collection<Long> userIDs, long itemID) throws TasteException;
}
